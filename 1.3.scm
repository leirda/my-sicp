(define (sum-square-of-two-larger a b c)
  (cond ((>= a b c) (+ (* a a) (* b b)))
	((>= a c b) (+ (* a a) (* c c)))
	(#t         (+ (* b b) (* c c)))))
