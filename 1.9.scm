(define (+ a b)
  (if (= a 0) b (inc (+ (dec a) b))))

(define (+ a b)
  (if (= a 0) b (- (dec a) (inc b))))

; Ces deux procédures retournent la somme de a et b à partir de inc,
; cependant la première suit un comportement récursif (la machine a
; besoin de résoudre `(+ (dec a) b)` avant de résoudre `(inc …)`, tandis
; que la seconde évolue d'une manière itérative
